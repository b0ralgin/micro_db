package repository

import (
	"bitbucket.org/b0ralgin/micro_API/article"
	pg "github.com/go-pg/pg"
)

func AddArticle(article *article.Article, db *pg.DB) error {
	_, err := db.Model(&article).
		Where("source = ? AND published_at = ?", article.Source, article.PublishedAt).
		OnConflict("DO NOTHING").
		SelectOrInsert()
	return err
}

func FindArticles(limit int, offset int, db *pg.DB) (*article.Articles, error) {
	articles := article.Articles{}
	err := db.Model(&articles.Articles).
		Order("published_at").
		Offset(offset * limit).
		Limit(limit).
		Select()
	return &articles, err
}

func FindArticlesBy(limit int, offset int, query string, db *pg.DB) (*article.Articles, error) {
	articles := article.Articles{}
	err := db.Model(&articles.Articles).
		Where("make_tsvector(title) @@ to_tsquery(?)", query).
		Order("published_at").
		Offset(offset * limit).
		Limit(limit).
		Select()
	return &articles, err
}
