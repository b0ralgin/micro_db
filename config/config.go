package config

import (
	"io/ioutil"

	yaml "gopkg.in/yaml.v2"
)

type Config struct {
	DB           string `yaml:"db"`
	LogMode      bool   `yaml:"log_mode"`
	NatsURL      string `yaml:"nats_url"`
	ArticleQueue string `yaml:"article_queue"`
}

func LoadCfg(fileName string) (*Config, error) {
	file, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, err
	}

	cfg := Config{}
	err = yaml.Unmarshal(file, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}
