package main

import (
	"flag"
	"log"
	"os"
	"time"

	article "bitbucket.org/b0ralgin/micro_API/article"
	"bitbucket.org/b0ralgin/micro_db/config"
	"bitbucket.org/b0ralgin/micro_db/controllers"
	pg "github.com/go-pg/pg"
	micro "github.com/micro/go-micro"
	"github.com/micro/go-plugins/broker/nats"
	natsio "github.com/nats-io/nats"
)

var (
	cnfFilename = flag.String("config", "config/config.yaml", "Filename of config file (YML format)")
)

func main() {
	flag.Parse()

	config, err := config.LoadCfg(*cnfFilename)
	if err != nil {
		log.Fatal("cannot parse config file:", err)
	}

	broker := nats.NewBroker(nats.Options(natsio.Options{
		Url: config.NatsURL,
	}))
	service := micro.NewService(
		micro.Name("go.micro.news"),
		micro.Broker(broker),
	)
	service.Init()

	db, err := openDB(config.DB, config.LogMode)

	dbController := controllers.NewDBController(db)
	article.RegisterArticleGetterHandler(service.Server(), dbController)
	err = micro.RegisterSubscriber(config.ArticleQueue, service.Server(), dbController.AddArticle)
	if err != nil {
		log.Fatal(err)
	}
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}

func openDB(conn string, debugMode bool) (*pg.DB, error) {
	opts, err := pg.ParseURL(conn)
	if err != nil {
		return nil, err
	}
	db := pg.Connect(opts)

	if debugMode {
		logger := log.New(os.Stdout, "db", log.LstdFlags)
		pg.SetLogger(logger)
	}
	var st time.Time
	_, err = db.QueryOne(pg.Scan(&st), "set timezone TO 'GMT'; SELECT now()")
	return db, err
}
