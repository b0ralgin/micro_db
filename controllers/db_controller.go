package controllers

import (
	"context"
	"log"

	article "bitbucket.org/b0ralgin/micro_API/article"
	"bitbucket.org/b0ralgin/micro_db/repository"
	pg "github.com/go-pg/pg"
)

type DBController struct {
	db      *pg.DB
	perPage int
}

func NewDBController(db *pg.DB) *DBController {
	return &DBController{
		db: db,
	}
}

func (d *DBController) GetArticles(ctx context.Context, req *article.Filter, rsp *article.Articles) error {
	if req.Search == "" {
		out, err := repository.FindArticles(int(req.PerPage), int(req.Page), d.db)
		rsp.Articles = append(rsp.Articles, out.Articles...)
		return err
	}
	out, err := repository.FindArticlesBy(d.perPage, int(req.Page), req.Search, d.db)
	rsp.Articles = append(rsp.Articles, out.Articles...)
	return err
}

func (d *DBController) AddArticle(ctx context.Context, req *article.Article) error {
	err := repository.AddArticle(req, d.db)
	if err != nil {
		log.Println(err)
	}
	return err
}
